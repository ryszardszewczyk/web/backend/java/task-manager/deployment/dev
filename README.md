# Deployment repository for local deployment with portainer on Windows.

Configuration set for local connection with Portainer.

### Setting up configuration

Before connecting to Portainer, you need to follow these steps.

### Directories

```bash
mkdir C:/docker-volume/01/volumes/config
```

### Configuration files

Before starting services, you need to clone to directory
```C:/docker-volume/01/volumes/config``` repository <https://gitlab.com/ryszardszewczyk/web/backend/java/task-manager/deployment/dev-config>

You need to create pem files in a directory ```C:/docker-volume/01/certs``` so auth-java service can load them in.

```shell
openssl genpkey -algorithm RSA -out private-key.pem
openssl rsa -pubout -in private-key.pem -out public-key.pem
```

### Docker network

Overlay network

```bash
docker network create -d overlay --attachable tma-dev-net
```
